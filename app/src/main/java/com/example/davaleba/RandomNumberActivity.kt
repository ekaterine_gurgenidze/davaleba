package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_random_number_layout.*

class RandomNumberActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_random_number_layout)
       init()
    }

    private fun init ()
    {
        generateRandomNumberButton.setOnClickListener{
            d("button","button is active!")
          //  randomNumberTextView.text="I am active"
            randomNumberGenerator()
        }
    }


    private fun randomNumberGenerator()
    {

        val n:Int =(-100..100).random()
        d("randomNumber","$n")
     if (n%5==0 && n/5>0) { randomNumberTextView.text="Yes"}
       else{
         randomNumberTextView.text="No"}

    }

}